"use strict"

var express = require('express'),
    router = express.Router(),
    _responseFactory = require('../factories/responseFactory'),
    productFacade = new (require('../facades/productFacade'))();

module.exports = function (app) {
  app.use('/product', router);
};

router.get('/', function(req, res){
	var responseFactory = _responseFactory(req.decoded);

	let skip = req.query.skip;
	let take = req.query.take;
	let _id = req.query._id;
	let name = req.query.name;
	let category = req.query.category;

	if(skip && take && !category)
		productFacade.getPaginated(
			skip,
			take,
			null,
			function(dbProducts) {
				res.json(responseFactory.success(dbProducts));
			}
		);

	if(skip && take && category)
		productFacade.getPaginated(
			skip,
			take,
			{'category': new RegExp(category, 'i')},
			function(dbProducts) {
				res.json(responseFactory.success(dbProducts));
			}
		);

	if(!skip && !take && _id)
		productFacade.get(
			{'_id': _id},
			function(dbProducts) {
				res.json(responseFactory.success(dbProducts[0]));
			}
		);

	if(!skip && !take && !_id && name)
		productFacade.get(
			{'name': new RegExp(name, 'i')},
			function(dbProducts) {
				res.json(responseFactory.success(dbProducts));
			}
		);

	if(!skip && !take && !_id && !name && category)
		productFacade.get(
			{'category': new RegExp(category, 'i')},
			function(dbProducts) {
				res.json(responseFactory.success(dbProducts));
			}
		);

	if(!skip && !take && !_id && !name && !category)
		productFacade.getAll(
			function(dbProducts) {
				res.json(responseFactory.success(dbProducts));
			}
		);
});