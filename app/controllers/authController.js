var express         = require('express'),
    router          = express.Router(),
    jwt             = require('jsonwebtoken'),
    crypto          = require('../../utils/cryptoManager'),
    config          = require('../../config/config'),
    _responseFactory = require('../factories/responseFactory'),
    userService     = require('../services/user.service'),
    mailFacade      = new (require('../facades/mailFacade'))();

var isInvalid = function(val) {
    return val == undefined || val == null || val == "";
};

module.exports = function (app) {
  app.use('/auth', router);
};


router.get('/',function(req, res) {
    var responseFactory = _responseFactory(req.decoded);

    clientEmail = req.query.email;
    clientPassword = req.query.password;

    _userService = new userService();

    _userService.get(
        { email: clientEmail },
        function(dbUser) {
            if (dbUser && dbUser.errors) throw dbUser;

            var user = dbUser[0];

            if (!user || user.password != crypto.hash(clientPassword)) {
                return res.json(responseFactory.fail(-1, 'Usuário inválido'));
            }

            user.password = null;
            
            var token = jwt.sign(user, config.tokenSecret, {"expiresIn": 60});
            
            res.json(responseFactory.success({
                            "token": token,
                            "user": user
                        }));
        }
    );
});

router.post('/',function(req, res) {
    var responseFactory = _responseFactory(req.decoded);
    
    var newUser = req.body;
    newUser.name = req.body.name;
    newUser.email = req.body.email;

    if(isInvalid(newUser.name)
        || isInvalid(newUser.email)
        || (isInvalid(newUser.password) || newUser.password.length < 6))
        return res.json(responseFactory.fail(-1, 'Usuário inválido'));

    newUser.password = crypto.hash(req.body.password); 
    newUser.date = new Date();

    _userService = new userService();

    _userService.add(
        newUser,
        function(dbUser) {
            if (dbUser && dbUser.code == 11000)
                return res.json(responseFactory.fail(-1,'Usuário existente'));
            if (dbUser && dbUser.errors)
                return res.json(responseFactory.fail(-1, 'Erro ao criar usuário'));
            
            dbUser.password = null;
            var token = jwt.sign(dbUser, config.tokenSecret, {"expiresIn": 60});
            
            mailFacade.sendWelcome(dbUser.email, dbUser.name);
            
            res.json(responseFactory.success({
                                "token": token,
                                "user": dbUser
                            }));
        }
    );
});

router.get('/checkToken',function(req, res) {
    var responseFactory = _responseFactory(req.decoded);
    
    var tokenData = {_doc: req.decoded};
    var token = jwt.sign(tokenData, config.tokenSecret, {"expiresIn": 60});
    
    res.json(responseFactory.success({
                        "token": token
                    }));
});