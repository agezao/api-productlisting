var config     = require('../../config/config'),
	request = require('request');

var mailBusiness = function() {
	'use strict'

	var mailBusiness = {
		'send': send
	}
	return mailBusiness;

	//////////

	function send(originMail, destinationMail, subject, htmlMessage) {
		var mailOptions = {
		    from: '"Gabriel Rodrigues" <'+ originMail +'>',
		    to: destinationMail,
		    subject: subject,
		    html: htmlMessage
		};

		console.log('sending mail');
		request.post(
		    'https://api:key-16d8498aac1a90c8a8d2ec2a3fa0eb86@api.mailgun.net/v3/mg.disparei.co/messages',
		    { formData: mailOptions },
		    function (error, response, body) {
		    	if(error)
		    		console.log(error);
		    }
		);
	}
};

module.exports = mailBusiness;