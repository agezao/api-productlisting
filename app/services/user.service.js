var mongoose = require('mongoose'),
    User = mongoose.model('User');


var userService = function() {
	'use strict'

	var parentMessageService = {
		'get': get,
		'add': add
	}
	return parentMessageService;

	//////////

	function get(params, callback) {
		if(typeof(params) === 'object')
			return User.find(params)
					.exec(function(err, result){
						if(err)
							if(callback)
								return callback(err);

						if(callback)
							return callback(result);
					});

		return User.findOne({_id: params})
				.exec(function(err, result){
						if(err)
							if(callback)
								return callback(err);

						if(callback)
							return callback(result);
					});
	}

	function add(viewModel, callback) {
		var obj = new User(viewModel);
		obj.save(function(err){
			if(err)
				if(callback)
					return callback(false);

			if(callback)
				callback(obj);
		});
	}
};

module.exports = userService;