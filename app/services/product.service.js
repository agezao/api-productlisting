var mongoose = require('mongoose'),
    Product = mongoose.model('Product');


var productService = function() {
	'use strict'

	var productService = {
		'get': get,
		'getPaginated': getPaginated,
		'add': add
	}
	return productService;

	//////////

	function get(params, callback) {
		if(!params)
			params = {};

		if(typeof(params) === 'object')
			return Product.find(params)
					.exec(function(err, result){
						if(err)
							if(callback)
								return callback(err);

						if(callback)
							return callback(result);
					});

		return Product.findOne({_id: params})
				.exec(function(err, result){
						if(err)
							if(callback)
								return callback(err);

						if(callback)
							return callback(result);
					});
	}

	function getPaginated(skip, take, params, callback) {
		if(!params)
			params = {};

		return Product.find(params)
				.limit(take)
			    .skip(skip)
			    .sort({
			        date: 'desc'
			    })
				.exec(function(err, result){
					if(err)
						if(callback)
							return callback(err);

					if(callback)
						return callback(result);
				});
	}

	function add(viewModel, callback) {
		var obj = new Product(viewModel);
		obj.save(function(err){
			if(err)
				if(callback)
					return callback(false);

			if(callback)
				callback(obj);
		});
	}
};

module.exports = productService;