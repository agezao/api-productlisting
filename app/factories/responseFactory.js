var config = require('../../config/config'),
    jwt    = require('jsonwebtoken');


var responseFactory = function(tokenData) {
    'use strict'

    var tokenBody = {_doc: tokenData};

    var responseFactory = {
        "fail": createFailedMessage,
        "success": createSucessMessage
    };
    return responseFactory;

    //////////

    function defaultResponseObject() {
        return {
            success: null,
            code:    null,
            message: null,
            data:    null
        };
    };

    function generateFreshToken() {
        var token = jwt.sign(tokenBody, config.tokenSecret, {"expiresIn": 60});

        return token;
    }

    function createFailedMessage(code, errorMessage){
        var responseObject = new defaultResponseObject();

        responseObject.success = false;
        responseObject.code    = code;
        responseObject.message = errorMessage;

        return responseObject;
    };

    function createSucessMessage(responseData){
        var responseObject = new defaultResponseObject();

        responseObject.success = true;
        responseObject.code    = 1;
        responseObject.data    = responseData;

        if(tokenBody._doc)
            responseObject.token = generateFreshToken();

        return responseObject;
    };
};

module.exports = responseFactory;