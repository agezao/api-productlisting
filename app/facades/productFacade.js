var productService = require('../services/product.service');

var productFacade = function() {
	'use strict'

	var productFacade = {
		'get': get,
		'getAll': getAll,
		'getPaginated': getPaginated
	}
	return productFacade;

	//////////

	function get(params, callback) {
		var _productService = new productService();
		_productService.get(params, callback);
	}

	function getAll(callback) {
		var _productService = new productService();
		_productService.get(null, callback);
	}

	function getPaginated(skip, take, params, callback) {
		var _productService = new productService();
		_productService.getPaginated(parseInt(skip), parseInt(take), params, callback);	
	}
};

module.exports = productFacade;