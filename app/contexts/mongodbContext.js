'use strict';

var config = require('../../config/config'),
	mongoose = require('mongoose'),
	single_connection;

var eventApplicationClose = function(){
	process.on('SIGINT', function(){
		mongoose.connection.close(function(){
		process.exit(0);
		});
	})
};

module.exports = function(uri){

	if(!uri)
		uri = config.db;

	if(!single_connection){
		mongoose.Promise = global.Promise;
		single_connection = mongoose.connect(uri);

		mongoose.connection.on('error',function(erro){
			console.log('MongoDb connection error ' + erro);
		});
	}

	eventApplicationClose();

	return single_connection;
};