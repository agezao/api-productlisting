var config = require('../config/config'),
    crypto = require("crypto");

var encrypt = function(data, isJson){
    if(isJson)
        data = JSON.stringify(data);

    var encipher = crypto.createCipheriv(config.cryptoAlgorithm, crypto.createHash('sha256').update(config.key).digest(), config.iv),
    encryptdata  = encipher.update(data, 'utf8', 'binary');

    encryptdata += encipher.final('binary');
    encode_encryptdata = new Buffer(encryptdata, 'binary').toString('base64');
    return encode_encryptdata;
};

var decrypt = function(data){
    encryptdata = new Buffer(data, 'base64').toString('binary');

    var decipher = crypto.createDecipheriv(config.cryptoAlgorithm, crypto.createHash('sha256').update(config.key).digest(), config.iv),
        decoded  = decipher.update(encryptdata, 'binary', 'utf8');
    

    decoded += decipher.final('utf8');
    return decoded;
};

var hash = function(data){
    return crypto.createHmac('sha1', config.key).update(data).digest('hex');
};

var cryptoManager = {
    "encrypt": encrypt,
    "decrypt": decrypt,
    "hash": hash
};

module.exports = cryptoManager;