var jwt = require('jsonwebtoken'),
    crypto = require('../utils/cryptoManager'),
    config = require('./config');

module.exports = function (app) {
	app.use(function(req, res, next) {
		
		if(req.path.indexOf("/auth") == 0 && req.path.indexOf("checkToken") < 0)
			return next();

	    var token = req.body.token || req.query.token || req.headers['token'];
	    
	    if (token) {
	        
	        jwt.verify(
				token, 
				config.tokenSecret,
				function(err, decoded) {      
					if (err) 
						return res.status(403).json({ success: false, data: 'Not authorized' });    

					else {
						req.decoded = decoded._doc;    
						next();
					}
	        	}
	        );
	    }
	    else
		    return res.status(403).send({ 
		        success: false, 
		        data: 'Not authorized' 
		    });
	});
};