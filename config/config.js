var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'test';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'apiproductlisting'
    },
    port: process.env.PORT || 3001,
    db: 'mongodb://localhost:27017/productlisting',
    key: 'SANGUE_LARANJA',
    tokenSecret: '004201533001004191',
    cryptoAlgorithm: 'aes-256-cbc',
    iv:'a2xhcgAAAAAAAAAA',
    appUrl: 'http://localhost:8080'
  },

  test: {
    root: rootPath,
    app: {
      name: 'apiproductlisting'
    },
    port: process.env.PORT || 3001,
    db: 'mongodb://api:12api34@ds119768.mlab.com:19768/productlisting',
    key: 'SANGUE_LARANJA',
    tokenSecret: '004201533001004191',
    cryptoAlgorithm: 'aes-256-cbc',
    iv:'a2xhcgAAAAAAAAAA',
    appUrl: 'http://localhost:8080'
  },

  production: {
    root: rootPath,
    app: {
      name: 'apiproductlisting'
    },
    port: process.env.PORT || 3001,
    db: 'mongodb://api:12api34@ds119768.mlab.com:19768/productlisting',
    key: 'SANGUE_LARANJA',
    tokenSecret: '004201533001004191',
    cryptoAlgorithm: 'aes-256-cbc',
    iv:'a2xhcgAAAAAAAAAA',
    appUrl: 'http://localhost:8080'
  }
};

module.exports = config[env];
