# API - Product Listing #

Here you have the API for the app ProductListing.

Build up using NodeJs & Express

By default the application will be listening to the MongoDb running on MongoLab:

* https://mlab.com/

If you wish to use local MongoDb for the app feel free to change the default config environment at /config/config.js

### ** How to run ** ###

* Clone or download the project
* npm install
* run npm start and wait until it finishes
* server will be available on localhost:3001

![App Architecture - Api.png](https://bitbucket.org/repo/4EGkbo/images/2845514343-App%20Architecture%20-%20Api.png)