var express = require('express'),
	config = require('./config/config'),
	glob = require('glob'),
	mongoCtx = require('./app/contexts/mongodbContext')();


var models = glob.sync(config.root + '/app/models/*.js');
models.forEach(function (model) {
  require(model);
});

var app = express();
app.disable('etag');

require('./config/express')(app, config);

app.listen(config.port, function () {
  console.log('Product Listing API running on ' + config.port);
});
